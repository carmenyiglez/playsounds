# Play Sounds #

## Integrantes del proyecto ##
* Cristian Alexis Abad de Vera y Carmen Yi González González.

## Información de los integrantes ##
* 2º Desarrollo de Aplicaciones Web.
* IES Las Galletas.
* Curso 2020/2021.

## Otros datos ##
* El proyecto consta de una página web en la que se ofrece a los usuarios la posibilidad de escuchar música de forma gratuíta y, además, les permitirá publicar su música y darse a conocer al público de manera sencilla. Asimismo, dicha página actuará como una red social en la que los usuarios puedan interactuar entre ellos.
* [Play Sounds](https://play-sounds.000webhostapp.com/)
