<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil Usuario</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!--Stylesheet CSS-->
    <link rel="stylesheet" href="./css/playsounds.css">

    <!--Favicon-->
    <link rel="icon" type="image/png" href="./images/logo_favicon.png">
</head>

<body>
    <div id="index-playsounds"></div>

    <nav id="navbar-style" class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="index.php"><img src="./images/logo.png" alt="PlaySounds Logo" width="100"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php
                session_start();

                if (!isset($_SESSION['user_id'])) {
                    echo ' <li class="nav-item">
                                    <a class="nav-link" href="login.php"><span class="color-navbar">Iniciar sesión</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="sign-up.php"><span class="color-navbar">Regístrate</span></a>
                                </li>';
                } else {
                    echo '<li class="nav-item">
                                    <a class="nav-link" href="logout.php"><span class="color-navbar">Cerrar sesión</span></a>
                                </li>';
                }
                ?>
                <?php
                if (isset($_SESSION['user_id'])) {
                    echo  '<li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="color-navbar"><i class="fas fa-user-circle"></i></span>
                                        </a>
                                    <div id="info-profile" class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="profile.php">Perfil</a>
                                        <a class="dropdown-item" href="my_songs.php">Mis canciones</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="logout.php">Cerrar sesión</a>
                                    </div>
                                </li>';
                }
                ?>
            </ul>
        </div>
    </nav>

    <?php include("connection.php");

    $username = $_GET["username"];

    //Información de la canción del usuario
    $stmt = $connection->prepare("SELECT title, album, genre, music.id FROM music INNER JOIN users ON music.user_id = users.id WHERE users.username = ?");

    $stmt->bindParam(1, $username);
    $stmt->execute();

    $my_songs = $stmt->fetchAll();
    $total_songs = $stmt->rowCount();

    $user_id = $_SESSION['user_id'];

    //Información del usuario
    $stmt = $connection->prepare("SELECT name, username FROM users INNER JOIN music ON users.id = music.user_id WHERE users.username = ?");

    $stmt->bindParam(1, $username);
    $stmt->execute();

    $user_info = $stmt->fetchAll();
    ?>

    <div class="row ml-2">
        <div class="col-md-2">
            <img src="./images/user_icon.png" alt="Foto de perfil" width="150">
            <h4><?php echo $user_info[0][0]; ?></h4>
            <h6><?php echo "@" . $user_info[0][1]; ?></h6>
        </div>
        <div class="col-md-2">
            <h6>10k Seguidores</h6>
        </div>
        <div class="col-md-2">
            <h6>10 seguidos</h6>
            <p>
                <button class="btn btn-success" style="width: 150px;"><i class="fas fa-user-plus"></i> Seguir</button>
                <button class="btn btn-light"><i class="fas fa-inbox"></i></button>
            </p>
        </div>
        <div class="col-md-2">
            <h6><?php echo $total_songs . " Canciones"; ?></h6>
        </div>
        <div id="songs-section" class="col-md-12">
            <div class="float-md-right">
                <input type="search" name="album" id="album-name" class="form-control" placeholder="Buscar por album...">
            </div>

            <form action="songs-section.php" method="POST">
                <?php

                foreach ($my_songs as $row) {
                    echo "<div id='info-mySongs' class='col-12 col-sm-12 col-md-12 box-music-mySongs'> 
                <p id='info-song'>
                    <button type='submit' name='music-id' value='" . $row['id'] . "' class='song-button'><i class='fas fa-play-circle'></i></button>
                    <strong>" . $row['title'] . "</strong> | <span class='album'>" . $row['album'] . "</span> <span><i>" . $row['genre'] . "</i></span>" .
                        "</p>
            </div>";
                }
                ?>
        </div>
    </div>

    <script type="text/javascript">
        const ALBUM_NAME = document.querySelector("#album-name");
        const SONGS = document.querySelectorAll("#info-mySongs");

        let arrAlbums = Array.from(SONGS);

        ALBUM_NAME.addEventListener("keyup", searchArtistAlbum);

        function searchArtistAlbum() {
            arrAlbums.filter(album => {
                if (album.querySelector("span.album").textContent.toLocaleLowerCase().includes(this.value.toLocaleLowerCase())) {
                    album.style.display = "block";
                } else {
                    album.style.display = "none";
                }
            });
        }
    </script>

    <!--JavaScript opcional-->
    <!--Primero JQuery, luego Popper.js, después Bootstrap JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>

</html>