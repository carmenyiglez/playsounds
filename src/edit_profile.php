<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi perfil</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<?php
include("connection.php");
session_start();

$user_id = $_SESSION['user_id'];

if ($_POST['action'] == "edit_profile") {
    if (isset($_POST['name']) || isset($_POST['username']) || isset($_POST['email'])) {
        if (!(empty($_POST['name']) || empty($_POST['username']) || empty($_POST['email']))) {
            $stmt = $connection->prepare("UPDATE users SET name = ?, username = ?, email = ? WHERE id = ?");

            $name = $_POST['name'];
            $username = $_POST['username'];
            $email = $_POST['email'];
            $stmt->bindParam(1, $name);
            $stmt->bindParam(2, $username);
            $stmt->bindParam(3, $email);
            $stmt->bindParam(4, $user_id);

            if ($stmt->execute()) {
                echo "<div class='alert alert-success' role='alert'>Se ha modificado con éxito.</div>";
            } else {
                echo "<div class='alert alert-danger' role='alert'>No se pudo actualizar.</div>";
            }
        } else {
            echo "<div class='alert alert-warning' role='alert'>Algunos de los campos se encuentran vacíos.</div>";
        }
    }
}

if ($_POST['action'] == "edit_password") {
    if (isset($_POST['passwd']) && !empty($_POST['passwd'])) {
        $stmt_pass = $connection->prepare("UPDATE users SET password = ? WHERE id = ?");

        $encrypted_password = md5($_POST['passwd']);
        $stmt_pass->bindParam(1, $encrypted_password);
        $stmt_pass->bindParam(2, $user_id);

        if ($stmt_pass->execute()) {
            echo "<div class='alert alert-success' role='alert'>Se ha modificado con éxito.</div>";
        } else {
            echo "<div class='alert alert-danger' role='alert'>No se pudo actualizar.</div>";
        }
    } else {
        echo "<div class='alert alert-warning' role='alert'>El campo de la contraseña se encuentra vacío.</div>";
    }
}

header('refresh:1.9;url=profile.php');
?>
</body>
</html>