<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Songs section</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!--Stylesheet CSS-->
    <link rel="stylesheet" href="./css/playsounds.css" type="text/css">

    <!--Favicon-->
    <link rel="icon" type="image/png" href="./images/logo_favicon.png">
</head>

<body>
    <nav id="navbar-style" class="navbar navbar-expand-lg navbar-light" style="width: 102%;">
        <a class="navbar-brand" href="index.php"><img src="./images/logo.png" alt="PlaySounds Logo" width="100"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php
                session_start();

                if (!isset($_SESSION['user_id'])) {
                    echo ' <li class="nav-item">
                                <a class="nav-link" href="login.php"><span class="color-navbar">Iniciar sesión</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="sign-up.php"><span class="color-navbar">Regístrate</span></a>
                            </li>';
                } else {
                    echo '<li class="nav-item">
                                <a class="nav-link" href="logout.php"><span class="color-navbar">Cerrar sesión</span></a>
                            </li>';
                }
                ?>
                <?php
                if (isset($_SESSION['user_id'])) {
                    echo  '<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="color-navbar"><i class="fas fa-user-circle"></i></span>
                                </a>
                            <div id="info-profile" class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="profile.php">Perfil</a>
                                <a class="dropdown-item" href="my_songs.php">Mis canciones</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="logout.php">Cerrar sesión</a>
                            </div>
                        </li>';
                }
                ?>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-md-6">
            <div class="card" style="width:22rem;">
                <!-- Card image -->
                <div class="view">
                    <img class="card-img-top" src="https://mdbootstrap.com/wp-content/uploads/2019/02/flam.jpg" alt="Card image cap">
                    <a href="" target="_blank">
                        <div class="mask gradient-card"></div>
                    </a>
                </div>

                <!-- Card content -->
                <div class="card-body text-center">
                    <?php
                    include("connection.php");

                    $stmt = $connection->prepare("SELECT title, album, genre, username FROM music INNER JOIN users ON music.user_id = users.id WHERE music.id = ?");
                    $music_id = $_POST['music-id'];
                    $stmt->bindParam(1, $music_id);
                    $stmt->execute();
                    $song_information = $stmt->fetchAll();

                    echo '
                        <h5 class="h5 font-weight-bold">Artista:<a href="./user_profile.php?username=' . $song_information[0][3] . '" target="_blank">' . $song_information[0][3] . '</a></h5>
                        <p class="mb-0"><strong>Género:</strong> ' . $song_information[0][2] . '</p>
                        <p class="mb-0"><strong>Album:</strong> ' . $song_information[0][1] . '</p>
                        <p class="mb-0"><strong>Título:</strong> ' . $song_information[0][0] . '</p>';

                    $song = "./songs/" . $_POST['music-id'];
                    echo "<audio controls>
                            <source src=" . $song . " type='audio/mpeg'>
                        </audio>";

                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div id="comentarios">
                <?php
                $stmt = $connection->prepare("SELECT comment, username FROM comments INNER JOIN users ON comments.user_comment_id = users.id WHERE comments.music_id = ?");
                $stmt->bindParam(1, $music_id);
                $stmt->execute();

                $comments = $stmt->fetchAll();
                foreach ($comments as $row) {
                    echo '<div id="comment"> 
                            <p>
                            <strong>@' . $row['username'] . '</strong>
                            <br>' .
                        $row['comment'] . '
                            <button type="submit" name="downvote" class="btn btn-danger"><i class="fas fa-thumbs-down"></i></button>
                            <button type="submit" name="upvote" class="btn btn-success"><i class="fas fa-thumbs-up"></i></button>
                            </p>
                        </div>';
                }
                ?>
            </div>

            <br>

            <form action="#" method="post">
                <div id="comentar">
                    <div class="form-group">
                        <input id="music-id" name="music-id" type="hidden" value="<?php echo $music_id; ?>">
                        <textarea name="comment" id="comment-id" cols="45" rows="2" placeholder="Comentar..."></textarea>
                    </div>
                    <?php
                    if (!isset($_SESSION['user_id'])) {
                        echo '<button type="submit" name="comentar" value="send_comment" class="btn btn-primary disabled-button" disabled>Comentar</button>';
                    } else {
                        echo '<button type="submit" name="comentar" value="send_comment" class="btn btn-primary">Comentar</button>';
                    }
                    ?>
                </div>
            </form>
        </div>
    </div>

    <br>

    <?php
    if (isset($_POST['comentar']) && !empty($_POST['comment'])) {
        if ($_POST['comentar'] == "send_comment") {
            try {
                $stmt = $connection->prepare("INSERT INTO comments (comment, user_comment_id, music_id) VALUES (?, ?, ?)");

                $comment = $_POST['comment'];
                $user_id = $_SESSION['user_id'];
                $music_id = $_POST['music-id'];

                $stmt->bindParam(1, $comment);
                $stmt->bindParam(2, $user_id);
                $stmt->bindParam(3, $music_id);

                if ($stmt->execute()) {
                    echo "<div class='alert alert-success message' role='alert'>Comentario añadido con éxito.</div>";
                }
            } catch (PDOException $ex) {
                die("<div class='alert alert-warning message' role='alert'><strong>¡ATENCIÓN!</strong>Se ha producido un error.</div>");
            }
        }
    }
    ?>

    <!--JavaScript opcional-->
    <!--Primero JQuery, luego Popper.js, después Bootstrap JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>

</html>