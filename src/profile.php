<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My profile</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!--Stylesheet CSS-->
    <link rel="stylesheet" href="./css/playsounds.css">

    <!--Favicon-->
    <link rel="icon" type="image/png" href="./images/logo_favicon.png">
</head>

<body>
    <nav id="navbar-style" class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="index.php"><img src="./images/logo.png" alt="PlaySounds Logo" width="100"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="logout.php"><span class="color-navbar">Cerrar sesión</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="color-navbar"><i class="fas fa-user-circle"></i></span>
                    </a>
                    <div id="info-profile" class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="my_songs.php">Mis canciones</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <?php
    session_start();
    $user_id = $_SESSION['user_id'];

    include("connection.php");
    $stmt = $connection->prepare("SELECT * FROM users WHERE id = $user_id");
    $stmt->execute();

    $user_information = $stmt->fetchAll();
    ?>

    <div class="row" style="width: 90%">
        <div class="col-md-7">
            <p>
            <form action="#" method="POST">
                <div class="profile-form profile-photo">
                    <p>
                        <img src="./images/user_icon.png" alt="Foto de perfil" width="150">
                    </p>

                    <p id="edit-image-form">
                        <input type="file" id="image" name="image">
                        <button type="submit" name="edit-image" value="edit_image" class="btn btn-warning">Guardar cambios</button>
                    </p>
                    <p>
                        <button name="changed-image" id="edit" class="btn btn-primary">Editar imagen</button>
                    </p>
                    <p>
                        <button type="submit" name="action" value="edit_password" class="btn btn-success" onclick="this.form.action='my_songs.php'">Mis canciones</button>
                    </p>
                </div>
            </form>
            </p>
        </div>
        <div class="col-md-4">
            <form action="edit_profile.php" method="POST">
                <div class="profile-form">
                    <h1>Mi perfil</h1>

                    <div class="form-group">
                        <label for="name-id">Nombre:</label>
                        <input type="text" name="name" id="name-id" class="form-control" value="<?php echo $user_information[0][3] ?>" placeholder="User">
                    </div>
                    <div class="form-group">
                        <label for="username-id">Nombre de usuario:</label>
                        <input type="text" name="username" id="username-id" class="form-control" value="<?php echo $user_information[0][1] ?>" placeholder="user123">
                    </div>
                    <div class="form-group">
                        <label for="username-id">Email del usuario:</label>
                        <input type="email" name="email" id="username-id" class="form-control" value="<?php echo $user_information[0][4] ?>" placeholder="user@email.com">
                    </div>
                    <div class="form-group">
                        <label for="passwd-id">Contraseña:</label>
                        <div class="input-group">
                            <input type="password" name="passwd" id="passwd-id" class="form-control" placeholder="**************">
                            <div class="input-group-append">
                                <button type="button" id="show" class="btn btn-primary"><i class="fas fa-eye"></i></button>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="action" value="edit_profile" class="btn btn-primary">Editar</button>
                    <button type="submit" name="action" value="edit_password" class="btn btn-success">Editar contraseña</button>
                </div>
            </form>
        </div>
    </div>

    <?php
    if (isset($_POST["edit-image"])) {
        $revisar = getimagesize($_FILES["image"]["tmp_name"]);
        if ($revisar !== false) {
            $image = $_FILES["image"]["name"];
            $imgContenido = addslashes(file_get_contents($image));
            try {
                $stmt = $connection->prepare("UPDATE users SET image = ? WHERE id = ?");

                $stmt->bindParam(1, $imgContenido);
                $stmt->bindParam(2, $user_id);

                if ($stmt->execute()) {
                    echo "<div class='alert alert-success message' role='alert'>Imagen subida con éxito.</div>";
                }
            } catch (PDOException $ex) {
                die("<div class='alert alert-warning message' role='alert'><strong>¡ATENCIÓN!</strong>Se ha producido un error.</div>");
            }
        } else {
            echo "Por favor seleccione imagen a subir.";
        }
    }
    ?>


    <script>
        const SHOW_PASSWORD = document.querySelector("#show");
        const PASSWORD = document.querySelector("#passwd-id");
        const EDIT_IMAGE = document.querySelector("button[name='changed-image']");
        const FORM_IMAGE_EDIT = document.querySelector("#edit-image-form");

        SHOW_PASSWORD.addEventListener("click", showPassword);

        let isHidden = false;

        function showPassword() {
            if (!isHidden) {
                PASSWORD.type = "text";
                SHOW_PASSWORD.className = "btn btn-danger";

                isHidden = true;
            } else {
                PASSWORD.type = "password";
                SHOW_PASSWORD.className = "btn btn-primary";

                isHidden = false;
            }
        }

        EDIT_IMAGE.addEventListener("click", showOptions);

        function showOptions(e) {
            e.preventDefault();

            FORM_IMAGE_EDIT.style.display = "block";
            EDIT_IMAGE.style.display = "none";
        }
    </script>

    <!--JavaScript opcional-->
    <!--Primero JQuery, luego Popper.js, después Bootstrap JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>

</html>