<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!--Stylesheet CSS-->
    <link rel="stylesheet" href="./css/playsounds.css" type="text/css">

    <!--Favicon-->
    <link rel="icon" type="image/png" href="./images/logo_favicon.png">
</head>

<body class="sign-up-body">
    <?php include("connection.php") ?>

    <nav id="navbar-style" class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="index.php"><img src="./images/logo.png" alt="PlaySounds Logo" width="100"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="login.php"><span class="color-navbar">Iniciar sesión</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sign-up.php"><span class="color-navbar">Regístrate</span></a>
                </li>
            </ul>
        </div>
    </nav>

    <form action="" method="POST">
        <div id="form">
            <h1>Sign Up</h1>

            <div class="form-group">
                <label for="name-id">Nombre:</label>
                <input type="text" name="name" id="name-id" class="form-control" placeholder="User">
            </div>
            <div class="form-group">
                <label for="username-id">Nombre de usuario:</label>
                <input type="text" name="username" id="username-id" class="form-control" placeholder="user123">
            </div>
            <div class="form-group">
                <label for="username-id">Email del usuario:</label>
                <input type="email" name="email" id="username-id" class="form-control" placeholder="user@email.com">
            </div>
            <div class="form-group">
                <label for="passwd-id">Contraseña:</label>
                <div class="input-group">
                    <input type="password" name="passwd" id="passwd-id" class="form-control">
                    <div class="input-group-append">
                        <button type="button" id="show" class="btn btn-primary"><i class="fas fa-eye"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="artist-id">Aceptar términos y condiciones</label>
                <input type="checkbox" name="terms" id="artist-id" value="yes">
            </div>
            <button type="submit" name="action" value="signup" id="edit" class="btn btn-primary" disabled>Registrarse</button>
            <button type="reset" name="action" value="reset" class="btn btn-success">Limpiar</button>
        </div>
    </form>

    <?php
    if (isset($_POST['username']) || isset($_POST['passwd']) || isset($_POST['action'])) {
        if (!empty($_POST['username'])) {
            if (!empty($_POST['passwd'])) {
                if (!empty($_POST['email'])) {

                    if ($_POST['action'] == "signup") {
                        //Comprobamos que el nombre de usuario no se repita
                        try {
                            $stmt = $connection->prepare("INSERT INTO users(name, username, email, password) VALUES (?, ?, ?, ?)");

                            $name = $_POST['name'];
                            $username = $_POST['username'];
                            $email = $_POST['email'];
                            $encrypted_password = md5($_POST['passwd']);

                            $stmt->bindParam(1, $name);
                            $stmt->bindParam(2, $username);
                            $stmt->bindParam(3, $email);
                            $stmt->bindParam(4, $encrypted_password);

                            if ($stmt->execute()) {
                                echo "<div class='alert alert-success message' role='alert'>Usuario creado con éxito.</div>";
                            }

                            //Envíamos al usuario a su perfil
                            $stmt = $connection->prepare("SELECT id, username, password FROM users where username = ? AND password = ?");

                            $stmt->bindParam(1, $username);
                            $stmt->bindParam(2, $encrypted_password);

                            $stmt->execute();

                            $all_users = $stmt->fetchAll();

                            if ($stmt->rowCount() >= 1) {
                                session_start();
                                $_SESSION['user_id'] = $all_users[0][0];

                                header("Location: profile.php");
                            }
                        } catch (PDOException $ex) {
                            die("<div class='alert alert-warning message' role='alert'><strong>¡ATENCIÓN!</strong> El nombre de usuario o el email se encuentra duplicado, o se ha producido otro error.</div>");
                        }
                    }
                } else {
                    echo "<div class='alert alert-warning message' role='alert'>" .
                        "Botón de envío no pulsado.</div>";
                }
            } else {
                echo "<div class='alert alert-warning' role='alert'>" .
                    "<strong>¡ATENCIÓN!</strong> El campo de la contraseña se encuentra vacío.</div>";
            }
        } else {
            echo "<div class='alert alert-warning' role='alert'>" .
                "<strong>¡ATENCIÓN!</strong> El campo del nombre de usuario se encuentra vacío.</div>";
        }
    }
    ?>

    <script type="text/javascript">
        const SHOW_PASSWORD = document.querySelector("#show");
        const PASSWORD = document.querySelector("#passwd-id");

        const CHECK_TERMS = document.querySelector("input[name='terms']");
        const EDIT = document.querySelector("#edit");

        SHOW_PASSWORD.addEventListener("click", showPassword);

        let isHidden = false;

        function showPassword() {
            if (!isHidden) {
                PASSWORD.type = "text";
                SHOW_PASSWORD.className = "btn btn-danger";

                isHidden = true;
            } else {
                PASSWORD.type = "password";
                SHOW_PASSWORD.className = "btn btn-primary";

                isHidden = false;
            }
        }

        CHECK_TERMS.addEventListener("click", isChecked);

        function isChecked() {
            if (CHECK_TERMS.checked) {
                EDIT.disabled = false;
            } else {
                EDIT.disabled = true;
            }
        }

        if (EDIT.disabled) {
            EDIT.className = "btn btn-primary disabled-button";
        }
    </script>

    <!--JavaScript opcional-->
    <!--Primero JQuery, luego Popper.js, después Bootstrap JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>

</html>