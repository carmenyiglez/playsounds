<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log out</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <style type="text/css">
        .message {
            width: 555px;
            text-align: center;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <?php
    session_start();
    session_destroy();
    echo "<div class='alert alert-success message'>Se ha cerrado sesión correctamente.</div>";
    header('refresh:1.9;url=index.php');
    ?>
</body>

</html>