<?php
$server = "localhost";
$user = "manager";
$password = "secret";
$db = "play_sounds";

$dsn = "mysql:host=$server;dbname=$db;charset=utf8mb4";

try {
    $connection = new PDO($dsn, $user, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $ex) {
    die("Error al conectarse a la base de datos. " . $ex->getMessage());
}
