<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mis Canciones</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!--Stylesheet CSS-->
    <link rel="stylesheet" href="./css/playsounds.css">

    <!--Favicon-->
    <link rel="icon" type="image/png" href="./images/logo_favicon.png">
</head>

<body>
    <nav id="navbar-style" class="navbar navbar-expand-lg navbar-light" style="width: 102%">
        <a class="navbar-brand" href="index.php"><img src="./images/logo.png" alt="PlaySounds Logo" width="100"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="logout.php"><span class="color-navbar">Cerrar sesión</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="color-navbar"><i class="fas fa-user-circle"></i></span>
                    </a>
                    <div id="info-profile" class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="profile.php">Perfil</a>
                        <a class="dropdown-item" href="my_songs.php">Mis canciones</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout.php">Cerrar sesión</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <?php 
    include("connection.php");

    session_start();

    $user_id = $_SESSION['user_id'];

    //Información de la canción del usuario
    $stmt = $connection->prepare("SELECT title, album, genre, music.id FROM music INNER JOIN users ON music.user_id = users.id WHERE users.id = ?");

    $stmt->bindParam(1, $user_id);
    $stmt->execute();

    $my_songs = $stmt->fetchAll();
    $total_songs = $stmt->rowCount();

    //Información del usuario
    $stmt = $connection->prepare("SELECT name, username FROM users WHERE id = ?");

    $stmt->bindParam(1, $user_id);
    $stmt->execute();

    $user_info = $stmt->fetchAll();
    ?>

    <div class="row ml-2">
        <div class="col-md-2">
            <img src="./images/user_icon.png" alt="Foto de perfil" width="150">
            <h4><?php echo $user_info[0][0]; ?></h4>
            <h6><?php echo "@" . $user_info[0][1]; ?></h6>
        </div>
        <div class="col-md-2">
            <h6>10k Seguidores</h6>
        </div>
        <div class="col-md-2">
            <h6>10 seguidos</h6>
        </div>
        <div class="col-md-2">
            <h6><?php echo $total_songs . " Canciones"; ?></h6>
        </div>
        <div class="col-md-2">
            <button style="border: none; background-color: transparent;"><i class="fas fa-bell"></i></button>
        </div>
        <div id="songs-section" class="col-md-12">
            <div class="float-md-right">
                <input type="search" name="album" id="album-name" class="form-control" placeholder="Buscar por album...">
            </div>

            <br><br>

            <form action="songs-section.php" method="POST">
                <?php
                $click = "this.form.action='my_songs.php'";
                foreach ($my_songs as $row) {
                    echo "<div id='info-mySongs' class='col-12 col-sm-12 col-md-12 box-music-mySongs'> 
                            <p id='info-song'>
                                <button type='submit' name='music-id' value='" . $row['id'] . "' class='song-button'><i class='fas fa-play-circle'></i></button>
                                <strong>" . $row['title'] . "</strong> | <span class='album'>" . $row['album'] . "</span> <span><i>" . $row['genre'] . "</i></span> 
                                <button type='submit' name='delete' onclick=" . $click . " value='" . $row['id'] . "'><i class='fas fa-times-circle'></i></button>" .
                        "</p>
                        </div>";
                }
                ?>
            </form>
            <div class="box-music-mySongs">
                <button id="add-song" style="border: none; background-color: transparent;"><i class="fas fa-plus"></i></button>
            </div>

            <div id="new-song-form">
                <form action="" method="POST" enctype="multipart/form-data">
                    <div>
                        <div class="form-group">
                            <label for="title-id">Título:</label>
                            <input type="text" name="title" id="title-id" class="form-control" placeholder="Título">
                        </div>
                        <div class="form-group">
                            <label for="album-id">Album:</label>
                            <input type="text" name="album" id="album-id" class="form-control" placeholder="Album">
                        </div>
                        <div class="form-group">
                            <label for="genre-id">Género:</label>
                            <select name="genre" id="genre" class="form-control">
                                <option value="Filtrar por género" disabled selected style="background-color:#DBDBDB;">Elige un género</option>
                                <option value="Pop">Pop</option>
                                <option value="Hip-Hop/Rap">Hip-Hop/Rap</option>
                                <option value="R&B/Soul">R&B/Soul</option>
                                <option value="Rock & Roll">Rock & Roll</option>
                                <option value="Electrónica">Electrónica</option>
                                <option value="Dance">Dance</option>
                                <option value="EDM">EDM</option>
                                <option value="Disco">Disco</option>
                                <option value="Salsa">Salsa</option>
                                <option value="Urbano Latino">Urbano Latino</option>
                                <option value="Alternativa">Alternativa</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="song">Elige la cancion: </label>
                            <input type="file" name="song" id="song">
                        </div>
                        <button type="submit" name="action" value="upload" id="subir-id" class="btn btn-primary">Subir canción</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
    if (isset($_POST['action']) && !empty($_POST['title']) && !empty($_POST['album']) && !empty($_POST['genre'])) {
        if ($_POST['action'] == "upload") {
            try {
                $stmt = $connection->prepare("INSERT INTO music(title, album, genre, user_id) VALUES (?, ?, ?, ?)");

                $title = $_POST['title'];
                $album = $_POST['album'];
                $genre = $_POST['genre'];

                $stmt->bindParam(1, $title);
                $stmt->bindParam(2, $album);
                $stmt->bindParam(3, $genre);
                $stmt->bindParam(4, $user_id);

                if ($stmt->execute()) {
                    echo "<div class='alert alert-success message' role='alert'>Cancion subida con éxito.</div>";
                    $lastId = $connection->lastInsertId();
                    $name = $_FILES["song"]["name"];
                    move_uploaded_file($_FILES["song"]["tmp_name"], "./songs/" . $lastId);
                    echo "<meta http-equiv=\"refresh\" content=\"0;URL=my_songs.php\">";
                }
            } catch (PDOException $ex) {
                die("<div class='alert alert-warning message' role='alert'><strong>¡ATENCIÓN!</strong>Se ha producido un error.</div>");
            }
        }
    }
    if (isset($_POST['delete'])) {
        try {
            $stmt = $connection->prepare("DELETE FROM music WHERE id = ?");

            $stmt->bindParam(1, $_POST['delete']);

            if ($stmt->execute()) {
                echo "<div class='alert alert-success message' role='alert'>Cancion eliminada con éxito.</div>";
                unlink('./songs/' . $_POST['delete']);
                echo "<meta http-equiv=\"refresh\" content=\"0;URL=my_songs.php\">";
            }
        } catch (PDOException $ex) {
            die("<div class='alert alert-warning message' role='alert'><strong>¡ATENCIÓN!</strong>Se ha producido un error.</div>");
        }
    }

    ?>

    <script type="text/javascript">
        const ADD_SONG = document.querySelector("#add-song");
        const NEW_SONG_FORM = document.querySelector("#new-song-form");
        const ALBUM_NAME = document.querySelector("#album-name");
        const SONGS = document.querySelectorAll("#info-mySongs");

        let arrAlbums = Array.from(SONGS);

        ADD_SONG.addEventListener("click", newSong);

        function newSong() {
            NEW_SONG_FORM.style.display = "block";
        }

        ALBUM_NAME.addEventListener("keyup", searchArtistAlbum);

        function searchArtistAlbum() {
            arrAlbums.filter(album => {
                if (album.querySelector("span.album").textContent.toLocaleLowerCase().includes(ALBUM_NAME.value.toLocaleLowerCase())) {
                    album.style.display = "block";
                } else {
                    album.style.display = "none";
                }
            });
        }
    </script>

    <!--JavaScript opcional-->
    <!--Primero JQuery, luego Popper.js, después Bootstrap JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>

</html>