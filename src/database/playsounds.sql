-- Creamos la base de datos
DROP DATABASE IF EXISTS play_sounds;
CREATE DATABASE IF NOT EXISTS play_sounds DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- Seleccionamos la base de datos "play_sounds"
USE play_sounds;

-- Creamos las tablas

-- Tabla "users"
CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    username VARCHAR(100) UNIQUE NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL
);

-- Tabla "music"
CREATE TABLE IF NOT EXISTS music (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    album VARCHAR(100) NOT NULL,
    genre VARCHAR(50) NOT NULL,
    user_id INT,
    CONSTRAINT fk_music_user FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Tabla "comments"
CREATE TABLE IF NOT EXISTS comments (
    id INT AUTO_INCREMENT PRIMARY KEY,
    comment VARCHAR(100) NOT NULL,
    user_comment_id INT,
    music_id INT,
    CONSTRAINT fk_comment_user FOREIGN KEY(user_comment_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_comment_music FOREIGN KEY(music_id) REFERENCES music(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Insertamos los usuarios
INSERT INTO users(name, username, email, password) VALUES ("Drake", "champagnepapi", "champagnepapi@gmail.com", "1234"), ("The Weeknd", "theweeknd", "theweeknd@yahoo.es", "1234"), ("Dua Lipa", "dualipa", "dualipa.hotmail.com", "1234"), ("Ariana Grande", "arianagrande", "arianagrande@republicrecords.net", "1234");

-- Insertamos algunas canciones en la tabla "music"
INSERT INTO music(title, album, genre, user_id) VALUES ("What's Next", "Scary Hours 2", "Hip-Hop/Rap", 1), ("Blinding Lights", "After Hours", "R&B/Soul", 2), ("Levitating", "Future Nostalgia", "Pop", 3), ("Pov", "Positions", "Pop", 4);

CREATE USER IF NOT EXISTS 'manager'@'localhost' IDENTIFIED BY "secret";

GRANT ALL ON play_sounds.* to 'manager'@'localhost';