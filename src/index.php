<?php
session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    <!--Stylesheet CSS-->
    <link rel="stylesheet" href="./css/playsounds.css" type="text/css">

    <!--Favicon-->
    <link rel="icon" type="image/png" href="./images/logo_favicon.png">
</head>

<body>
    <?php include("connection.php"); ?>

    <div id="index-playsounds"></div>

    <nav id="navbar-style" class="navbar navbar-expand-lg navbar-light" style="width: 102%;">
        <a class="navbar-brand" href="index.php"><img src="./images/logo.png" alt="PlaySounds Logo" width="100"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php
                if (!isset($_SESSION['user_id'])) {
                    echo ' <li class="nav-item">
                                <a class="nav-link" href="login.php"><span class="color-navbar">Iniciar sesión</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="sign-up.php"><span class="color-navbar">Regístrate</span></a>
                            </li>';
                } else {
                    echo '<li class="nav-item">
                                <a class="nav-link" href="logout.php"><span class="color-navbar">Cerrar sesión</span></a>
                            </li>';
                }
                ?>
                <?php
                if (isset($_SESSION['user_id'])) {
                    echo  '<li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="color-navbar"><i class="fas fa-user-circle"></i></span>
                                    </a>
                                <div id="info-profile" class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="profile.php">Perfil</a>
                                    <a class="dropdown-item" href="my_songs.php">Mis canciones</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="logout.php">Cerrar sesión</a>
                                </div>
                            </li>';
                }
                ?>
            </ul>
        </div>
    </nav>

    <?php
    $stmt = $connection->prepare("SELECT * FROM music");
    $stmt->execute();

    $all_musics = $stmt->fetchAll();

    //Últimas 4 canciones 
    $stmt = $connection->prepare("SELECT * FROM music ORDER BY music.id DESC LIMIT 4");
    $stmt->execute();

    $last_novelties = $stmt->fetchAll();
    ?>

    <!--Main-->
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <h2>Novedades</h2>
        </div>
    </div>
    <div id="music-novelties" class="row">
    <?php 
        foreach ($last_novelties as $row) {
            echo "<div class=\"d-block offset-md-1\"></div>";
            echo "<div class=\"col-4 col-sm-3 col-md-1 mr-5 mt-2\">" .
            "<div class=\"box-music\">" .
                "<span>" .
                    "<form action=\"songs-section.php\" method=\"POST\">";
                        echo "<button type=\"submit\" name=\"music-id\" value=\"" . $row['id'] . "\" class=\"song-button novelty-music\"><i class='fas fa-play-circle'></i></button>";
                    echo "</form>" .
                "</span>" .
            "</div>" .
        "</div>";
        } 
        ?>
    </div>

    <br>

    <div class="row mt-5">
        <div class="col-12 col-sm-12 col-md-3">
            <h2>Canciones</h2>
        </div>
        <div class="col-12 col-sm-6 col-md-4">
            <select name="genre-music" id="genre" class="form-control">
                <option value="Filtrar por género" disabled selected style="background-color:#DBDBDB;">Filtrar por género</option>
                <option value="Pop">Pop</option>
                <option value="Hip-Hop/Rap">Hip-Hop/Rap</option>
                <option value="R&B/Soul">R&B/Soul</option>
                <option value="Rock & Roll">Rock & Roll</option>
                <option value="Electrónica">Electrónica</option>
                <option value="Dance">Dance</option>
                <option value="EDM">EDM</option>
                <option value="Disco">Disco</option>
                <option value="Salsa">Salsa</option>
                <option value="Urbano Latino">Urbano Latino</option>
                <option value="Alternativa">Alternativa</option>
            </select>
        </div>
        <div class="col-12 col-sm-6 col-md-2">
            <input type="search" name="song-name" id="name" class="form-control" placeholder="Buscar canción...">
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <form action="songs-section.php" method="POST">
                <div class="row">
                    <?php
                    foreach ($all_musics as $row) {
                        echo "<div class='col-12 col-sm-12 col-md-12 mt-3 song'> 
                            <p id='info-song'>
                                <button type='submit' name='music-id' value='" . $row['id'] . "' class='song-button'><i class='fas fa-play-circle'></i></button>
                                <strong>" . $row['title'] . "</strong> | " . $row['album'] . " <span><i>" . $row['genre'] . "</i></span>" .
                            "</p>
                        </div>";
                    }
                    ?>
                </div>
            </form>
        </div>
    </div>

    <div class="row section" style="width: 103%;">
        <div class="col-12 col-sm-12 col-md-12">
            <h3 id="index-message" class="text-center">Gracias por confiar en nosotros</h3>
        </div>
    </div>

    <!--Footer-->
    <div class="row">
        <div class="col-12 col-sm-12 col-md-4 p-5 text-light footer" style="border-bottom: 1px solid #fff;">
            <h5>Play Sounds</h5>
            <p>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-facebook"></i></a>
            </p>
        </div>
        <div class="col-12 col-sm-12 col-md-4 p-5 text-light footer" style="border-bottom: 1px solid #fff;">
            <h4>Explorar</h4>
            <div class="text-secondary">
                <a href="#"><p>Eventos</p></a>
                <a href="#"><p>Charts</p></a>
                <a href="#"><p>Número 1 del mes</p></a>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 p-5 text-light footer" style="border-bottom: 1px solid #fff;">
            <h4>¿Quiénes somos?</h4>
            <div class="text-secondary">
                <a href="https://bitbucket.org/carmenyiglez/playsounds/src/master/"><p>Desarrolladores</p></a>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 p-5 text-light footer">
            <div id="img-logo">
                <a href="#index-playsounds"><img src="./images/logo.png" alt="Play Sounds Logo" width="250" class="d-block mx-auto"></a>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        const GENRES = document.querySelector("#genre");
        const NAMES = document.querySelector("#name");
        const SONGS = document.querySelectorAll(".song");

        let arrSongs = Array.from(SONGS);

        GENRES.addEventListener("change", filterGenres);

        function filterGenres() {
            arrSongs.filter(filterSong => {
                if (filterSong.querySelector("span>i").textContent.includes(GENRES.value)) { //Si el género del desplegable se encuentra en la sección de canciones
                    filterSong.style.display = "block"; //se muestra
                } else {
                    filterSong.style.display = "none"; //De lo contrario, se oculta/n
                }
            });
        }

        console.log(NAMES);
        NAMES.addEventListener("keyup", searchSongsName);

        function searchSongsName() {
            arrSongs.filter(filterName => {
                if (filterName.querySelector("strong").textContent.toLocaleLowerCase().includes(this.value.toLocaleLowerCase())) {
                    filterName.style.display = "block";
                } else {
                    filterName.style.display = "none";
                }
            });
        }
    </script>

    <!--JavaScript opcional-->
    <!--Primero JQuery, luego Popper.js, después Bootstrap JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>

<!--
    Las canciones que se encuentran por defecto, junto con los usuarios, no son los mismos en el proyecto local que los del hosting, ya que se nos complicaba un poco a la hora de subir una canción mediante un fichero SQL.
    Además de que, al ser en local, a otra persona que usara el proyecto se le borraría (si solo se pueden subir dichas canciones mediante código, en este caso, PHP)
-->

</html>